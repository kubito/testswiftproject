//
//  CountryDetailVC.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 27/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import AAInfographics

class CountryDetailVC: UIViewController {

    // MARK: - Variables
    private let scrollView = UIScrollView()
    private let contentView = UIStackView()
    private let confirmedData = DataView(title: "Confirmed")
    private let recoveredData = DataView(title: "Recovered")
    private let deathsData = DataView(title: "Deaths")
    private let recoveredDeathsChart = AAChartView()
    private let activeChart = AAChartView()
    private let vm: CountryDetailVMType
    private var placeholder: Placeholder?
    private let disposeBag = DisposeBag()

    // MARK: - Initializers

    init(vm: CountryDetailVMType) {
        self.vm = vm
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupContent()
        bind()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm.loadData()
    }

    // MARK: - Content

    private func setupContent() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)

        contentView.axis = .vertical

        scrollView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        contentView.snp.makeConstraints { make in
            make.edges.width.equalToSuperview()
        }

        contentView.addArrangedSubview(confirmedData)
        contentView.addArrangedSubview(recoveredData)
        contentView.addArrangedSubview(deathsData)
        contentView.addArrangedSubview(recoveredDeathsChart)
        contentView.addArrangedSubview(activeChart)

        recoveredDeathsChart.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(contentView.snp.width).dividedBy(1.5)
        }

        activeChart.snp.makeConstraints { make in
            make.width.equalToSuperview()
            make.height.equalTo(contentView.snp.width).dividedBy(1.5)
        }
    }

    // MARK: - Binding

    private func bind() {
        vm.content
            .asObservable()
            .subscribe(onNext: { [weak self] content in
                guard let content = content else { return }

                self?.title = content.country
                self?.confirmedData.customizet(with: DataView.Content(total: content.confirmed.total, new: content.confirmed.new))
                self?.recoveredData.customizet(with: DataView.Content(total: content.recovered.total, new: content.recovered.new))
                self?.deathsData.customizet(with: DataView.Content(total: content.deaths.total, new: content.deaths.new))
                if let recoveredDeathsData = content.recoveredDeathsChartData {
                    self?.recoveredDeathsChart.isHidden = false
                    self?.recoveredDeathsChart.aa_drawChartWithChartModel(recoveredDeathsData)
                } else {
                    self?.recoveredDeathsChart.isHidden = true
                }
                if let activeData = content.activeChartData {
                    self?.activeChart.aa_drawChartWithChartModel(activeData)
                    self?.activeChart.isHidden = false
                } else {
                    self?.activeChart.isHidden = true
                }
            }).disposed(by: disposeBag)

        vm.placeholder
            .asObservable()
            .subscribe(onNext: { [weak self] placeholder in
                self?.resolvePlaceholder(placeholder)
            })
            .disposed(by: disposeBag)
    }

    private func resolvePlaceholder(_ placeholderType: PlaceholerType?) {
        guard let placeholderType = placeholderType else {
            placeholder?.removeFromSuperview()
            placeholder = nil
            return
        }

        let placeholder = self.placeholder ?? Placeholder()

        if placeholder.superview == nil {
            view.addSubview(placeholder)
            placeholder.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
            placeholder.buttonTapped.subscribe(onNext: { [weak self] _ in
                self?.vm.loadData()
            }).disposed(by: disposeBag)
        }

        placeholder.customize(with: placeholderType)
        self.placeholder = placeholder
    }
}
