//
//  CountryDetailVM.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 27/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import AAInfographics

protocol CountryDetailVMType {
    var content: BehaviorRelay<CountryDetailVM.Content?> { get }
    var placeholder: BehaviorRelay<PlaceholerType?> { get }

    func loadData()
}

class CountryDetailVM: CountryDetailVMType {

    // MARK: - Variables
    // MARK: private
    private let country: CountryDTO
    private let dataProvider: CountryDetailDataProviderType

    // MARK: public

    lazy var content: BehaviorRelay<CountryDetailVM.Content?> =  .init(value: Content(country: country.country,
                                                                                      recovered: (country.totalRecovered, country.newRecovered),
                                                                                      deaths: (country.totalDeaths, country.newDeaths),
                                                                                      confirmed: (country.totalConfirmed, country.newConfirmed),
                                                                                      recoveredDeathsChartData: nil,
                                                                                      activeChartData: nil))
    let placeholder: BehaviorRelay<PlaceholerType?> = .init(value: nil)

    // MARK: - Init

    init(country: CountryDTO, dataProvider: CountryDetailDataProviderType) {
        self.country = country
        self.dataProvider = dataProvider
    }

    // MARK: - Functions
    // MARK: public

    func loadData() {
        dataProvider.load(country: country.countryCode).done { [weak self] data in
            self?.processResponse(data)
            self?.placeholder.accept(nil)
        }.catch { [weak self] _ in
            self?.placeholder.accept(.networkError)
        }
    }

    // MARK: private

    private func processResponse(_ data: [CountryDetailDTO]) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateStyle = .short
        let categories = data.map({ dateFormatter.string(from: $0.date) })
        let recoveredDeathsChartData: AAChartModel? = basicChartModel(title: "Recovered/Deaths")
            .categories(categories)
            .series([
                AASeriesElement()
                    .name("Recovered")
                    .data(data.map({ $0.recovered})),
                AASeriesElement()
                    .name("Deaths")
                    .data(data.map({ $0.deaths}))
                ])
        let activeChartData: AAChartModel? = basicChartModel(title: "Active cases")
            .categories(categories)
            .series([
                AASeriesElement()
                    .name("Active")
                    .data(data.map({ $0.active}))
            ])

        content.accept(Content(country: country.country,
                               recovered: (country.totalRecovered, country.newRecovered),
                               deaths: (country.totalDeaths, country.newDeaths),
                               confirmed: (country.totalConfirmed, country.newConfirmed),
                               recoveredDeathsChartData: recoveredDeathsChartData,
                               activeChartData: activeChartData))
    }

    private func basicChartModel(title: String) -> AAChartModel {
        AAChartModel()
            .chartType(.spline)
            .animationType(.elastic)
            .title(title)
    }

    // MARK: - Content

    struct Content {
        let country: String
        let recovered: (total: Int, new: Int)
        let deaths: (total: Int, new: Int)
        let confirmed: (total: Int, new: Int)
        let recoveredDeathsChartData: AAChartModel?
        let activeChartData: AAChartModel?
    }
}
