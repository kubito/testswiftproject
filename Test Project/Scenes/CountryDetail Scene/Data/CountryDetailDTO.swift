//
//  CountryDetailDTO.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 27/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation

struct CountryDetailDTO: Codable {
    let country: String
    let confirmed: Int
    let deaths: Int
    let recovered: Int
    let active: Int
    let date: Date

    enum CodingKeys: String, CodingKey {
        case country = "Country"
        case confirmed = "Confirmed"
        case deaths = "Deaths"
        case recovered = "Recovered"
        case active = "Active"
        case date = "Date"
    }
}
