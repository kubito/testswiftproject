//
//  CountryDetailDataProvider.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 27/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import PromiseKit

protocol CountryDetailDataProviderType {
    func load(country: String) -> Promise<[CountryDetailDTO]>
}

class CountryDetailDataProvider: CountryDetailDataProviderType {

    private let apiClient: ApiClient

    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }

    func load(country: String) -> Promise<[CountryDetailDTO]> {
        Promise { seal in
            let router = Router(path: "total/dayone/country/\(country)", method: .get)
            do {
                try apiClient.request(router: router).responseDecodable().done({ (data: [CountryDetailDTO]) in
                    seal.fulfill(data)
                }).catch({ error in
                    seal.reject(error)
                })
            } catch {
                seal.reject(error)
            }
        }
    }
}
