//
//  DataView.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 02/09/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import UIKit

class DataView: UIView {

    // MARK: - Variable
    private let containerView = UIView()
    private let title = UILabel()
    private let total = UILabel()
    private let new = UILabel()

    // MARK: - Initializers

    init(title: String) {
        super.init(frame: .zero)
        setupContent()
        self.title.text = title
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Content

    private func setupContent() {
        view.addSubview(containerView)
        containerView.layer.cornerRadius = 2.0
        containerView.backgroundColor = .lightGray

        containerView.addSubview(title)
        containerView.addSubview(total)
        containerView.addSubview(new)

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(10)
        }

        title.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview().inset(10)
        }

        total.snp.makeConstraints { make in
            make.top.equalTo(title.snp.bottom)
            make.leading.equalTo(title)
            make.width.equalTo(title).dividedBy(2)
            make.bottom.equalToSuperview().inset(10)
        }

        new.snp.makeConstraints { make in
            make.leading.equalTo(total.snp.trailing)
            make.width.equalTo(title).dividedBy(2)
            make.bottom.equalToSuperview().inset(10)
        }
    }

    func customizet(with content: Content) {
        total.text = "Total: \(content.total)"
        new.text = "New: \(content.new)"
    }

    struct Content {
        let total: Int
        let new: Int
    }
}
