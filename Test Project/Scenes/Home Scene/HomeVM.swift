//
//  HomeVM.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 25/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources
import RxCocoa

protocol HomeVMType {
    var content: BehaviorRelay<[HomeSectionContent]> { get }
    var placeholder: BehaviorRelay<PlaceholerType?> { get }

    func loadData()
}

class HomeVM: HomeVMType {

    // MARK: - Variables
    // MARK: private

    private let dataProvider: HomePageDataProviderType

    // MARK: public

    let content: BehaviorRelay<[HomeSectionContent]> = .init(value: [])
    let placeholder: BehaviorRelay<PlaceholerType?> = .init(value: nil)

    // MARK: - Initializer

    init(dataProvider: HomePageDataProviderType) {
        self.dataProvider = dataProvider
    }

    // MARK: - Function

    func loadData() {
        if content.value.isEmpty {
            placeholder.accept(.loading)
        }

        dataProvider.load().done { [weak self] summary in
            guard let sSelf = self else { return }

            let groupedCountries = Dictionary(grouping: summary.countries, by: { "\($0.country.first ?? "A")" })
            var sectionContent = groupedCountries.map({ key, value -> HomeSectionContent in
                let countries = value.sorted(by: { $0.country < $1.country })
                return HomeSectionContent(
                    items: countries.map({ HomeContent(title: "\(sSelf.getFlag(for: $0.countryCode)) \($0.country)", countryCode: $0.countryCode, dto: $0) }),
                    title: key)
            })
            sectionContent = sectionContent.sorted(by: { $0.title < $1.title })
            sSelf.content.accept(sectionContent)
            sSelf.placeholder.accept(nil)
        }.catch { [weak self] _ in
            self?.content.accept([])
            self?.placeholder.accept(.networkError)
        }
    }

    private func getFlag(for countryCode: String) -> String {
        let base: UInt32 = 127397
        var s = ""
        for v in countryCode.unicodeScalars {
            if let unicodeScalarsForLetter = UnicodeScalar(base + v.value) {
                s.unicodeScalars.append(unicodeScalarsForLetter)
            }
        }
        return String(s)
    }
}

struct HomeContent {
    let title: String
    let countryCode: String
    let dto: CountryDTO
}

struct HomeSectionContent: SectionModelType {
    typealias Item = HomeContent

    let title: String
    var items: [HomeContent]

    init(original: HomeSectionContent, items: [HomeContent]) {
        self.items = items
        self.title = original.title
    }

    init(items: [HomeContent], title: String) {
        self.title = title
        self.items = items
    }
}
