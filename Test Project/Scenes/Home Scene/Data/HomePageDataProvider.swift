//
//  HomePageDataProvider.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 26/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

protocol HomePageDataProviderType {
    func load() -> Promise<SummaryDTO>
}

class HomePageDataProvider: HomePageDataProviderType {

    private let apiClient: ApiClient

    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }

    func load() -> Promise<SummaryDTO> {
        Promise { seal in
            let router = Router(path: "summary", method: .get)
            do {
                try apiClient.request(router: router).responseDecodable().done { (data: SummaryDTO) in
                    seal.fulfill(data)
                }.catch({ error in
                    seal.reject(error)
                })
            } catch {
                seal.reject(error)
            }
        }
    }
}
