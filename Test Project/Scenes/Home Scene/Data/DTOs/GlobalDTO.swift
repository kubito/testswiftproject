//
//  GlobalDTO.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 01/09/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation

struct GlobalDTO: Codable {
    let newConfirmed: Int
    let totalConfirmed: Int
    let newDeaths: Int
    let totalDeaths: Int
    let newRecovered: Int
    let totalRecovered: Int

    enum CodingKeys: String, CodingKey {
        case newConfirmed = "NewConfirmed"
        case totalConfirmed = "TotalConfirmed"
        case newDeaths = "NewDeaths"
        case totalDeaths = "TotalDeaths"
        case newRecovered = "NewRecovered"
        case totalRecovered = "TotalRecovered"
    }
}
