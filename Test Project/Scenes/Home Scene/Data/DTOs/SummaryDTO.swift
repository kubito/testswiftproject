//
//  SummaryDTO.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 01/09/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation

struct SummaryDTO: Codable {
    let global: GlobalDTO
    let countries: [CountryDTO]
    let date: Date

    enum CodingKeys: String, CodingKey {
        case global = "Global"
        case countries = "Countries"
        case date = "Date"
    }
}
