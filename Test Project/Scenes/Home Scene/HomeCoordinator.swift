//
//  HomeCoordinator.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 20/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import XCoordinator
import Swinject

enum HomeRoute: Route {
    case list, detail(CountryDTO)
}

class HomeCoordinator: NavigationCoordinator<HomeRoute> {

    private let container: Swinject.Container

    init(container: Swinject.Container) {
        self.container = container
        super.init(initialRoute: .list)
    }

    override func prepareTransition(for route: HomeRoute) -> NavigationTransition {
        switch route {
        case .list:
            guard let vm = container.resolve(HomeVMType.self) else { return .none() }
            let vc = HomeVC(vm: vm, detailNavigation: unownedRouter)
            return .push(vc)
        case .detail(let country):
            guard let vm = container.resolve(CountryDetailVMType.self, argument: country) else { return .none() }
            let vc = CountryDetailVC(vm: vm)
            return .push(vc)
        }
    }
}
