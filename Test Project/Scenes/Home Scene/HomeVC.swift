//
//  HomeViewController.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 20/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxDataSources
import XCoordinator

class HomeVC: UIViewController {

    // MARK: - Variables
    // MARK: private

    private let tableView = UITableView()
    private let vm: HomeVMType
    private let detailNavigation: UnownedRouter<HomeRoute>
    private let disposeBag = DisposeBag()
    private var placeholder: Placeholder?
    private lazy var dataSource = RxTableViewSectionedReloadDataSource<HomeSectionContent>(configureCell: { dataSource, tableView, _, item -> UITableViewCell in
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") ?? UITableViewCell()
        cell.textLabel?.text = item.title
        return cell
    }, titleForHeaderInSection: { dataSource, index in
        return dataSource.sectionModels[index].title
    })

    // MARK: - Init

    init(vm: HomeVMType, detailNavigation: UnownedRouter<HomeRoute>) {
        self.vm = vm
        self.detailNavigation = detailNavigation
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"

        setupContent()
        bind()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: selectedIndexPath, animated: true)
        }

        vm.loadData()
    }

    // MARK: - Content

    private func setupContent() {
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        tableView.tableFooterView = UIView()
        tableView.rowHeight = 50
        tableView.sectionHeaderHeight = 40

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    // MARK: - Binding

    private func bind() {
        vm.content
            .asDriver()
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        vm.placeholder
            .asObservable()
            .subscribe(onNext: { [weak self] placholder in
                self?.resolvePlaceholder(placholder)
            })
            .disposed(by: disposeBag)

        tableView.rx
            .modelSelected(HomeContent.self).subscribe(onNext: { [weak self] content in
                self?.onModelSelected(content: content)
            })
            .disposed(by: disposeBag)
    }

    private func onModelSelected(content: HomeContent) {
        detailNavigation.trigger(.detail(content.dto))
    }

    private func resolvePlaceholder(_ placeholderType: PlaceholerType?) {
        guard let placeholderType = placeholderType else {
            placeholder?.removeFromSuperview()
            placeholder = nil
            return
        }

        let placeholder = self.placeholder ?? Placeholder()

        if placeholder.superview == nil {
            view.addSubview(placeholder)
            placeholder.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
            placeholder.buttonTapped.subscribe(onNext: { [weak self] _ in
                self?.vm.loadData()
            }).disposed(by: disposeBag)
        }

        placeholder.customize(with: placeholderType)
        self.placeholder = placeholder
    }
}
