//
//  ApiClient.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 26/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {

    // MARK: - Variables

    private let baseURL: String
    private let manager = Alamofire.Session.default

    // MARK: - Initializer

    public init(baseURL: String) {
        self.baseURL = baseURL
    }

    // MARK: - Functions

    public func request(router: RouterType) throws -> DataRequest {
        let request = try getRequest(from: router)
        return manager.request(request).validate()
    }

    // MARK: private

    private func getRequest(from router: RouterType) throws -> URLRequest {
        let url = try baseURL.asURL().appendingPathComponent(router.path)
        var request = URLRequest(url: url)
        request.method = router.method

        return request
    }
}
