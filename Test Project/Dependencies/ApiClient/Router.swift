//
//  Router.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 26/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Alamofire
import Foundation

public protocol RouterType {
    /// String of request path
    var path: String { get }

    /// HTTPMethod (post, get, put, delete)
    var method: HTTPMethod { get }

    /// [String: Any] disctionary of parameters - optional
    var parameters: Parameters? { get }

    /// [String: String] disctionary of http headers - optional
    var httpHeaders: HTTPHeaders? { get }

    /// Data of http body - optional
    var httpBody: Data? { get }
}

public extension RouterType {
    var parameters: Parameters? { return nil }
    var httpHeaders: HTTPHeaders? { return nil }
    var httpBody: Data? { return nil }
}

public struct Router: RouterType {
    public let path: String
    public let method: HTTPMethod
    public let parameters: Parameters?
    public let httpHeaders: HTTPHeaders?
    public let httpBody: Data?

    /**
     Initializer for create Router

     - Parameter path:          String of request path
     - Parameter method:        HTTPMethod (post, get, put, delete)
     - Parameter parameters:    [String: Any] disctionary of parameters - optional
     - Parameter httpHeaders:   [String: String] disctionary of http headers - optional
     - Parameter httpBody:      Data of http body - optional

    */
    public init(path: String, method: HTTPMethod, parameters: Parameters? = nil, httpHeaders: HTTPHeaders? = nil, httpBody: Data? = nil) {
        self.path = path
        self.method = method
        self.parameters = parameters
        self.httpHeaders = httpHeaders
        self.httpBody = httpBody
    }
}
