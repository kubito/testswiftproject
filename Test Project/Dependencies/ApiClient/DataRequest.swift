//
//  DataRequest.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 26/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

extension DataRequest {

    func responseDecodable<T: Decodable>(queue: DispatchQueue = .main, decoder: JSONDecoder = JSONDecoder()) -> Promise<T> {
        Promise { seal in
            responseData(queue: queue) { response in
                switch response.result {
                case .success(let value):
                    do {
                        decoder.dateDecodingStrategy = .iso8601
                        seal.fulfill(try decoder.decode(T.self, from: value))
                    } catch {
                        print("\(#file):\(#line) \(error)")
                        seal.reject(error)
                    }
                case .failure(let error):
                    print("\(#file):\(#line) \(error)")
                    seal.reject(error)
                }
            }
        }
    }
}
