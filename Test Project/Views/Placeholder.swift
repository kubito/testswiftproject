//
//  Placeholder.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 27/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import RxSwift
import RxCocoa

enum PlaceholerType {
    case networkError
    case noData
    case loading
}

class Placeholder: UIView {

    // MARK: - Variables
    // MARK: private

    private let stackView = UIStackView()
    private lazy var titleLabel = UILabel()
    private lazy var descriptionLabel = UILabel()
    private lazy var button = UIButton()
    private lazy var activityIndicator = UIActivityIndicatorView(style: .gray)
    private let disposeBag = DisposeBag()

    // MARK: public

    let buttonTapped = PublishSubject<Void>()

    // MARK: - Initializers

    init() {
        super.init(frame: .zero)
        setupContent()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Content

    private func setupContent() {
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        titleLabel.textAlignment = .center
        descriptionLabel.font = UIFont.systemFont(ofSize: 13)
        descriptionLabel.textAlignment = .center

        button.setTitleColor(.black, for: .normal)
        button.setTitleColor(.gray, for: .highlighted)
        button.rx.tap
            .bind(to: buttonTapped)
            .disposed(by: disposeBag)

        stackView.axis = .vertical
        stackView.alignment = .center
        stackView.spacing = 10

        view.addSubview(stackView)
        stackView.alignment = .fill
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func setupNoData() {
        stackView.removeAllSubviews()

        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)

        titleLabel.text = "No data"
        descriptionLabel.text = "There are no data for this."
    }

    private func setupNetworkError() {
        stackView.removeAllSubviews()

        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(button)

        titleLabel.text = "Network error"
        descriptionLabel.text = "Check your network connection and try it again"
        button.setTitle("try it again".uppercased(), for: .normal)
    }

    private func setupLoading() {
        stackView.removeAllSubviews()

        stackView.addArrangedSubview(activityIndicator)
        activityIndicator.startAnimating()
    }

    public func customize(with placeholder: PlaceholerType) {
        switch placeholder {
        case .networkError:
            setupNetworkError()
        case .noData:
            setupNoData()
        case .loading:
            setupLoading()
        }
    }
}

extension UIStackView {
    func removeAllSubviews() {
        arrangedSubviews.forEach { view in
            removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}
