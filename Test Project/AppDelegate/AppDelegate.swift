//
//  AppDelegate.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 20/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: - Variables
    // MARK: private

    private let container = Container()
    private lazy var mainWindow = UIWindow()
    private lazy var router = HomeCoordinator(container: self.container).strongRouter

    // MARK: - Application life cycle

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        applyStyle()

        // Init dependencies
        initDependencies(container: container)

        // Set up AppCoordinator
        router.setRoot(for: mainWindow)

        return true
    }

    func applyStyle() {
        UINavigationBar.appearance().prefersLargeTitles = true
    }
}
