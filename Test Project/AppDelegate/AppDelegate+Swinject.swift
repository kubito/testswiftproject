//
//  AppDelegate+Swinject.swift
//  Test Project
//
//  Created by Jakub Ruzicka on 20/08/2020.
//  Copyright © 2020 Jakub Ruzicka. All rights reserved.
//

import Foundation
import Swinject

extension AppDelegate {

    func initDependencies(container: Container) {
        // General
        container.register(ApiClient.self) { _ in
            ApiClient(baseURL: "https://api.covid19api.com")
        }

        // HomePage
        container.register(HomePageDataProviderType.self) { resolver -> HomePageDataProviderType in
            guard let apiClient = resolver.resolve(ApiClient.self) else {
                fatalError()
            }
            return HomePageDataProvider(apiClient: apiClient)
        }
        container.register(HomeVMType.self) { resolver -> HomeVMType in
            guard let dataProvider = resolver.resolve(HomePageDataProviderType.self) else {
                fatalError()
            }

            return HomeVM(dataProvider: dataProvider)
        }

        // CountryDetail
        container.register(CountryDetailDataProviderType.self) { resolver -> CountryDetailDataProviderType in
            guard let apiClient = resolver.resolve(ApiClient.self) else {
                fatalError()
            }
            return CountryDetailDataProvider(apiClient: apiClient)
        }
        container.register(CountryDetailVMType.self) { (resolver, country: CountryDTO) -> CountryDetailVMType in
            guard let dataProvider = resolver.resolve(CountryDetailDataProviderType.self) else {
                fatalError()
            }
            return CountryDetailVM(country: country, dataProvider: dataProvider)
        }
    }
}
